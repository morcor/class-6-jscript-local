/**
 * Car class
 * @constructor
 * @param {String} model
 */
  //  Create an instance, accelerate twice, brake once, and console log the instance.toString()
  class Car {
    constructor(model){
      this.currentSpeed = 0;
      this.model = model;
    }

    accelerate() {
      this.currentSpeed += 1;
      console.log(`Increasing speed by 1 - new speed is ${this.currentSpeed} mph`);
    }

    brake() {
      this.currentSpeed -= 1;
      console.log(`Decreasing speed by 1 - new speed is ${this.currentSpeed} mph`);
    }

    toString() {
      console.log(`${this.model} is currently going ${this.currentSpeed} mph`);
    }
  };

 
  const tacoma = new Car("Toyota");

 tacoma.accelerate();
 tacoma.accelerate();
 tacoma.brake();
 tacoma.toString();
  

/**
 * ElectricCar class
 * @constructor
 * @param {String} model
 */

//  Create an instance, accelerate twice, brake once, and console log the instance.toString()


class electricCar {
    constructor(electricModel) {
      this.currentSpeed = 0;
      this.electricModel = electricModel;
    }
    accelerateElectricCar() {
    this.currentSpeed += 1;
    console.log(`Increasing speed by 1 - new speed is ${this.currentSpeed} mph`);
  }

  brakeElectricCar() {
    this.currentSpeed -= 1;
    console.log(`Decreasing speed by 1 - new speed is ${this.currentSpeed} mph`);
  }

  toStringElectricCar() {
    console.log(`${this.electricModel} is currently going ${this.currentSpeed} mph`);
  }

};


const tesla = new electricCar("Tesla");

 tesla.accelerateElectricCar();
 tesla.accelerateElectricCar();
 tesla.brakeElectricCar();
 tesla.toStringElectricCar();