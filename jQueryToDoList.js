/**
 * Toggles "done" class on <li> element
 */

$(document).on('click','li', function(e) {let$this=$(this);$(this).toggleClass('done');})

/**
 * Delete element when delete link clicked
 */

$(document).on('click','.delete', function(){
  $(this).parent().fadeOut();
});




/**
 * Adds new list item to <ul>
 */

const addListItem = function(e) {
  e.preventDefault();
  const text = $('#new-todo').val();
  const $newListItem = `<li> <span> ${text} </span> <a class='delete'> Delete </a> </li>`;
  $('.today-list').append($newListItem);
  
  
};

  
  // rest here...


// add listener for add
$('.add-item').click(addListItem);

