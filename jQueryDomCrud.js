// Create a new <a> element containing the text "Buy Now!"
// with an id of "cta" after the last <p>

const $newAnchor = $('<a>')
$newAnchor.text("Buy Now!");
$newAnchor.attr("id", "cta");

$('main').append($newAnchor)

$newAnchor.click(function () {
    this.remove();
    $('main').append($('p').text('added to cart!'));
})

// Access (read) the data-color attribute of the <img>,
// log to the console

const $img = $('img')
console.log($img.data('color'))


// Update the third <li> item ("Turbocharged"),
// set the class name to "highlight"

const $li = $('li')

$("li:nth-child(3)").addClass("highlight");

// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")

// Create a listener on the "Buy Now!" link that responds to a click event.
// When clicked, the the "Buy Now!" link should be removed
// and replaced with text that says "Added to cart"

$newAnchor.on("click", function() {console.log(this, "has been Clicked")})


